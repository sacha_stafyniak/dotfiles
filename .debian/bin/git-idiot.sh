#!/bin/bash

~/.debian/env.sh

checkIdiotFolder() {
  IDIOT_FOLDER=$1; shift

  pushd $IDIOT_FOLDER &>/dev/null
  project=`basename $(pwd)`
  branch=`git rev-parse --symbolic-full-name --abbrev-ref HEAD`
  changes=`git status -s | wc -l`

  echo -n "[git-idiot][$project][$branch] $changes changes detected ..."
  if [ "$changes" == "0" ]; then
    echo " skipping"
  else
    echo -n " updating ..."

    if ! [[ $branch == $IDIOT_BRANCH_PREFIX* ]]; then
      git checkout -b "${IDIOT_BRANCH_PREFIX}${branch}"
    fi

    git add . &>/dev/null
    git commit -m "$IDIOT_MESSAGE" &>/dev/null
    git push &>/dev/null

    echo " done"
  fi
  popd &>/dev/null
}


# Check if a human is active here since last hour
idleTime=`$HOME/.debian/bin/getIdle`

if ! [[ $IDIOT_IDLE_THRESHOLD -lt $idleTime ]]; then
  echo "[git-idiot] skipping: idle threshold not reached"
  exit 1
fi

if [ -f "$IDIOT_FILE" ]; then
  echo "[git-idiot] checking $(cat $IDIOT_FILE | wc -l) idiots folders:"
  while read path; do
    checkIdiotFolder $path
  done <$IDIOT_FILE
fi
