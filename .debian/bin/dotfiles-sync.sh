#!/bin/bash

local dot="/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME"

if [[ `$dot status -s | wc -l` == "0" ]]; then
  echo "[dotfiles] up-to-date"
else
  echo -n "[dotfiles] synchronizing uncommited files ..."
  $dot commit -am "[sync] auto update"
  $dot push
  echo " done"
fi

