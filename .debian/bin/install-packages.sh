#!/bin/bash

if [[ $UID != 0 ]]; then
  echo "Please run this script with sudo:"
  echo "sudo $0 $*"
  exit 1
fi 

if [ -f "/home/$SUDO_USER/.debian/packages.lock" ]; then
  dpkg --clear-selections
  dpkg --set-selections < /home/$SUDO_USER/.debian/packages.lock
fi

