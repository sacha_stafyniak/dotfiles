CHANGES=`dot st | wc -l`
WHATCH_COUNT=`cat $HOME/.debian/git-idiot.conf | wc -l`

read -r -d '' MOTD << EndOfMotd
Watching $WHATCH_COUNT git folder(s) with git-idiot
There are $CHANGES unsaved changes in dotfiles
EndOfMotd

echo "$MOTD\n"

