function git-idiot {
  source ~/.debian/env.sh
  touch $IDIOT_FILE

  case $1 in
  add-this-folder-because-i-cannot-handle-by-myself|add|a)
    if [ -d "$(pwd)/.git" ]; then
      grep -q -F "$(pwd)" $IDIOT_FILE || echo $(pwd) >> $IDIOT_FILE
      echo "Successfully added \"$(pwd)\" to git-idiot watching list"
    else
      echo "You are an idiot, this is not a git repository"
      return 42
    fi
    ;;
  remove-watcher-here-and-i-promise-i-ll-be-carefull|rm|r|d)
    if [ -d "$(pwd)/.git" ]; then    
      grep -v "^$(pwd)$" $IDIOT_FILE > "${IDIOT_FILE}.tmp" && mv -f "${IDIOT_FILE}.tmp" $IDIOT_FILE || truncate -s 0 $IDIOT_FILE
      echo "Successfully removed \"$(pwd)\" from git-idiot watching list, be carefull"
    else
      echo "You are an idiot, you really need me, this is not a git repository"
      return 42
    fi
    ;;
  merge-what-you-saved-for-me-please|m)
    local project=$2;
    local project_path
    if [[ "$project" == "" ]]; then
      echo "You are an idiot, you need to tell me which project I need to merge what I saved for you"
      return 42
    fi

    while read project_path; do
      pushd $project_path &>/dev/null
      local curr_project=$(basename $(pwd))

      if ! [[ "$curr_project" == "$project" ]]; then 
        popd &>/dev/null
        continue
      fi

      local saved_branch=`git rev-parse --symbolic-full-name --abbrev-ref HEAD`
      local unsaved_changes=$(git status -s | wc -l)

      if ! [[ $saved_branch == $IDIOT_BRANCH_PREFIX* ]]; then 
        echo "You are an idiot, this project is up-to-date"

        popd &>/dev/null
        return 42
      fi

      if ! [[ "$unsaved_changes" == "0" ]]; then 
        echo "You are an idiot, you have $unsaved_changes unsaved changes ..."

        popd &>/dev/null
        return 42
      fi

      local original_branch=${saved_branch/$IDIOT_BRANCH_PREFIX/}
      echo "Merging $curr_project from $saved_branch to $original_branch ..."
      git checkout $original_branch &>/dev/null
      git merge $saved_branch &>/dev/null

      if ! [[ $? == 0 ]]; then 
        echo "[error] something went wrong when auto-merging updates\n"
        echo "You have to manualy resolve conflicts:"
        echo "  cd $project_path"
        echo "  ... do your merge"
        echo "  git commit\n"

        echo "Then press any key to push && clean idiot branch\n"
        read
      fi;

      git push &>/dev/null
      
      # remove old branch
      git branch -D $saved_branch &>/dev/null
      git push origin ":$saved_branch" &>/dev/null

      echo "Done baby!"
      popd &>/dev/null
      return 0
    done <$IDIOT_FILE

    echo "Project \"$project\" not found, you idiot"
    return 42
    ;;
  list-me-please-where-the-fucking-repos-are|ls|l)
    #cat $IDIOT_FILE
    local project_path
    local -a details
    details=("\e[1mProject,Branch,Wip,Path\e[0m")
    while read project_path; do
      pushd $project_path &>/dev/null
      local project=$(basename $(pwd))
      local branch=$(git rev-parse --symbolic-full-name --abbrev-ref HEAD)
      local changes=$(git status -s | wc -l)
      if [[ $changes -gt 0 ]]; then
        changes="\e[1;33m${changes}*\e[0m"
      fi
      if [[ $branch == $IDIOT_BRANCH_PREFIX* ]]; then
        branch="\e[1;31m☠\e[0m\xC2\xA0\e[1m${branch}\e[0m"
      fi
      details+="$project,$branch,$changes,$project_path"
      popd &>/dev/null
    done <$IDIOT_FILE
    echo $details | tr ' ' '\n' | column -s ',' -t
    ;;
  rtfm|*)
    cat << EndOfRTFM
[git-idiot] toolsuite to auto save work in progress from git projects, because u idiot and lazy

Usage: git-idiot [option]

Options:
 [a]dd-this-folder-because-i-cannot-handle-by-myself - add current path to watched repos
 [d]emove-watcher-and-i-promise-i-ll-be-carefull - remove current path from watched repos
 [l]ist-me-please-where-the-fucking-repos-are - display watched repositories
 rtfm - display this help (do not try [r] shortcut, it's same as [d])
EndOfRTFM
  esac
}

alias gi=git-idiot
alias dot='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'
alias dc=docker-compose

if [ -d "$HOME/.nvm" ]; then
  export NVM_DIR="$HOME/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
  [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
fi

