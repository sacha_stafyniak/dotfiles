#!/bin/bash

IDIOT_BRANCH_PREFIX="idiot-wip-"
IDIOT_FILE="$HOME/.debian/git-idiot.conf"
IDIOT_MESSAGE="[git-idiot] auto saved wip"
IDIOT_IDLE_THRESHOLD=`echo $(( 1000 * 60 * 30 ))` # one hour

PACKAGES_LOCKFILE="$HOME/.debian/packages.lock"
