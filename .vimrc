syntax on

set encoding=utf-8
set nowrap
set nu
set mouse=r

set tabstop=2 shiftwidth=2 expandtab
autocmd FileType make setlocal noexpandtab

set listchars=tab:→\ ,space:·,nbsp:☠,trail:•,eol:¶,precedes:«,extends:»

