# Global configuration
export TERM="xterm-256color"
export VISUAL=vim
export EDITOR="$VISUAL"
export PATH=~/.local/bin:$PATH

# Antigen ZSH Package manager configuration
#ANTIGEN_CACHE=false
source ~/antigen.zsh

antigen use oh-my-zsh

antigen bundle aws
antigen bundle common-aliases
antigen bundle debian
antigen bundle docker 
antigen bundle git
antigen bundle node
antigen bundle npm  
antigen bundle vscode
antigen bundle web-search
antigen bundle z

antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-completions
antigen bundle zsh-users/zsh-autosuggestions

# Theme customisation
POWERLEVEL9K_INSTALLATION_PATH=$ANTIGEN_BUNDLES/bhilburn/powerlevel9k

POWERLEVEL9K_MODE='awesome-patched'
POWERLEVEL9K_SHORTEN_DIR_LENGTH=2
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(dir)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status vcs nvm node_version)

POWERLEVEL9K_DIR_HOME_FOREGROUND="white"
POWERLEVEL9K_DIR_HOME_SUBFOLDER_FOREGROUND="white"
POWERLEVEL9K_NODE_VERSION_FOREGROUND="black"
POWERLEVEL9K_DIR_DEFAULT_FOREGROUND="white"

antigen theme bhilburn/powerlevel9k powerlevel9k

antigen apply

# Load extra aliases
if [ -f ~/.debian/aliases.zsh ]; then
  . ~/.debian/aliases.zsh
fi

if [ -f ~/.debian/motd.zsh ]; then
  . ~/.debian/motd.zsh
fi

